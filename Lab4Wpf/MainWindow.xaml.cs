﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.SolverFoundation.Common;
using Microsoft.SolverFoundation.Services;
using Microsoft.SolverFoundation.Solvers;

namespace Lab4Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //создаем регулярное выражение, описывающее правило ввода
        //в данном случае, это символы от 1 до 4
        Regex inputRegex = new Regex(@"^[2-4]$");
        int[] column_del, row_del;
        int column, row;
        DataTable dtMatrix;

        public MainWindow()
        {
            InitializeComponent();

            InitializeEvent();
        }

        private void InitializeEvent()
        {
            textRow.PreviewTextInput += Text_PreviewTextInput;
            textColumn.PreviewTextInput += Text_PreviewTextInput;

            btnPrepare.Click += BtnPrepare_Click;
            btnExample.Click += BtnExample_Click;
            btnMatch.Click += BtnMatch_Click;
        }

        private bool CanDelRow(double[][] matrix, int i, int j)
        {
            for (int k = 0; k < column; k++)
                if (matrix[i][k] >= matrix[j][k])
                    continue;
                else
                    return false;
            return true;
        }

        private bool CanDelColumn(double[][] matrix, int i, int j)
        {
            for (int k = 0; k < row; k++)
                if (matrix[k][i] <= matrix[k][j])
                    continue;
                else
                    return false;
            return true;
        }

        private void BtnMatch_Click(object sender, RoutedEventArgs e)
        {
            column_del = new int[column];
            row_del = new int[row];

            textLog.Text = "";
            double z = GetMin();

            double[][] matrix = new double[row][];
            for (int i = 0; i < row; i++)
            {
                matrix[i] = new double[column];
                for (int j = 0; j < column; j++)
                {
                    matrix[i][j] = Double.Parse(dtMatrix.Rows[i][j + 1].ToString()) - z;
                }
            }

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    if (i == j || row_del[i] == -1 || row_del[j] == -1)
                        continue;
                    if (CanDelRow(matrix, i, j))
                        row_del[j] = -1;

                }
            }

            for (int i = 0; i < column; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    if (i == j || column_del[i] == -1 || column_del[j] == -1)
                        continue;
                    if (CanDelColumn(matrix, i, j))
                        column_del[j] = -1;

                }
            }

            if (column_del.Min() >= 0 && row_del.Min() >= 0)
                textLog.Text += "Простить матрицу не удалось\n";
            else
                textLog.Text += "Упрощение матрицы\n";
            if (row_del.Min() < 0)
                for (int i = 0; i < row; i++)
                    if (row_del[i] < 0)
                        textLog.Text += "Стратегия X #" + (i + 1) + " исключена из рассмотрения\n";
            if (column_del.Min() < 0)
                for (int i = 0; i < column; i++)
                    if (column_del[i] < 0)
                        textLog.Text += "Стратегия Y #" + (i + 1) + " исключена из рассмотрения\n";


            double maxmin = MaxMin(), minmax = MinMax();
            int maxminof = MaxMinOf(), minmaxof = MinMaxOf();

            textLog.Text += "Нижняя цена игры \n";
            textLog.Text += "a = " + maxmin + "\n";
            textLog.Text += "Верхняя цена игры \n";
            textLog.Text += "b = " + minmax + "\n";

            if (maxmin == minmax)
            {
                textLog.Text += "Так как a == b, то решением игры являются оптимальные стратегии игроков и цена игры\n";
                textLog.Text += "\nОТВЕТ\n";
                textLog.Text += "Цена игры = " + maxmin + "\n";
                textLog.Text += "Оптимальная стратегия 1-го игрока " + (maxminof + 1) + "\n";
                textLog.Text += "Оптимальная стратегия 2-го игрока " + (minmaxof + 1) + "\n";
            }
            else if (maxmin <= minmax)
            {
                textLog.Text += "Так как a <= b, то решением являются смешанные стратегии\n";
                textLog.Text += "Решаем задачу ЛП\n";

                double[] x = GetMinimize(matrix);
                double[] y = GetMaximize(matrix);
                double price = 1 / x.Sum();

                textLog.Text += "\nОТВЕТ\n";
                textLog.Text += String.Format("Цена игры равна {0:F3}\n", price + z);
                textLog.Text += "\n";

                for (int i = 0; i < row; i++)
                {
                    x[i] *= price;
                    textLog.Text += String.Format("p{0:D1} {1:F3}\n", i + 1, x[i]);
                }
                textLog.Text += "\n";
                
                for (int i = 0; i < column; i++)
                {
                    y[i] *= price;
                    textLog.Text += String.Format("q{0:D1} {1:F3}\n", i + 1, y[i]);
                }
                textLog.Text += "\n";

            }
            else
            {
                textLog.Text += "Так как a > b, то ошибка!";
                return;
            }
        }

        private double[] GetMaximize(double[][] matrix)
        {
            SimplexSolver solver = new SimplexSolver();

            int[] y = new int[column];
            for (int i = 0; i < column; i++)
            {
                solver.AddVariable("Y" + i, out y[i]);
                solver.SetLowerBound(y[i], 0);
            }

            int[] a = new int[row];
            int cost;
            for (int i = 0; i < row; i++)
            {
                solver.AddRow("row" + i, out a[i]);
                for (int j = 0; j < column; j++)
                {
                    solver.SetCoefficient(a[i], y[j], matrix[i][j]);
                }
                solver.SetUpperBound(a[i], 1);
            }
            solver.AddRow("cost", out cost);
            for (int j = 0; j < column; j++)
            {
                solver.SetCoefficient(cost, y[j], 1);
            }

            solver.AddGoal(cost, 1, false);

            solver.Solve(new SimplexSolverParams());

            double[] temp = new double[column];

            for (int j = 0; j < column; j++)
            {
                temp[j] = solver.GetValue(y[j]).ToDouble();
            }

            return temp;
        }

        private double[] GetMinimize(double[][] matrix)
        {
            SimplexSolver solver = new SimplexSolver();

            int[] x = new int[row];
            for (int i = 0; i < row; i++)
            {
                solver.AddVariable("X" + i, out x[i]);
                solver.SetLowerBound(x[i], 0);
            }

            int[] a = new int[column];
            int cost;
            for (int i = 0; i < column; i++)
            {
                solver.AddRow("column" + i, out a[i]);
                for (int j = 0; j < row; j++)
                {
                    solver.SetCoefficient(a[i], x[j], matrix[j][i]);
                }
                solver.SetLowerBound(a[i], 1);
            }
            solver.AddRow("cost", out cost);
            for (int j = 0; j < row; j++)
            {
                solver.SetCoefficient(cost, x[j], 1);
            }

            solver.AddGoal(cost, 1, true);

            solver.Solve(new SimplexSolverParams());

            double[] temp = new double[row];

            for (int j = 0; j < row; j++)
            {
                temp[j] = solver.GetValue(x[j]).ToDouble();
            }

            return temp;
        }

        private void BtnExample_Click(object sender, RoutedEventArgs e)
        {
            textRow.Text = "3";
            textColumn.Text = "3";

            BtnPrepare_Click(null, null);

            string[,] matrix = {
                {"4", "1", "6" },
                {"-1", "2", "3" },
                {"-6", "-3", "4" }
            };

            for (int i = 0; i < column; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    dtMatrix.Rows[i][j + 1] = matrix[i, j];
                }
            }
        }

        private void BtnPrepare_Click(object sender, RoutedEventArgs e)
        {
            textLog.Text = "";
            if (Int32.TryParse(textRow.Text, out row) && Int32.TryParse(textColumn.Text, out column))
            {
                dtMatrix = new DataTable();

                dtMatrix.Columns.Clear();
                dtMatrix.Columns.Add("#");
                for (int i = 0; i < column; i++)
                {
                    dtMatrix.Columns.Add("Y #" + (i + 1));
                }

                dtMatrix.Rows.Clear();
                for (int i = 0; i < row; i++)
                {
                    dtMatrix.Rows.Add("X #" + (i + 1));
                }

                dtMatrix.Columns[0].ReadOnly = true;

                dataMatrix.DataContext = dtMatrix.DefaultView;
            }
            else
            {
                MessageBox.Show("Неправильно задано количество строк и столбцов!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Text_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //проверяем или подходит введенный символ нашему правилу
            Match match = inputRegex.Match(e.Text);
            //и проверяем или выполняется условие
            //если количество символов в строке больше или равно одному либо
            //если введенный символ не подходит нашему правилу
            if ((sender as TextBox).Text.Length >= 1 || !match.Success)
            {
                //то обработка события прекращается и ввода неправильного символа не происходит
                e.Handled = true;
            }
        }

        private double GetMin()
        {
            double[] min = new double[row];
            for (int i = 0; i < row; i++)
            {
                min[i] = RowToArray(i).Min();
            }
            if (min.Min() < 0)
                return min.Min();
            else
                return 0;
        }

        private int MaxMinOf()
        {
            double[] min = new double[row];
            for (int i = 0; i < row; i++)
            {
                min[i] = RowToArray(i).Min();
            }
            return Array.IndexOf(min, min.Max());
        }

        private int MinMaxOf()
        {
            double[] max = new double[column];
            for (int i = 0; i < column; i++)
            {
                max[i] = ColumnToArray(i).Max();
            }
            return Array.IndexOf(max, max.Min());
        }

        private double MaxMin()
        {
            double[] min = new double[row];
            for (int i = 0; i < row; i++)
            {
                min[i] = RowToArray(i).Min();
            }
            return min.Max();
        }

        private double MinMax()
        {
            double[] max = new double[column];
            for (int i = 0; i < column; i++)
            {
                max[i] = ColumnToArray(i).Max();
            }
            return max.Min();
        }

        private double[] RowToArray(int i)
        {
            double[] row = new double[this.column];
            for (int j = 0; j < this.column; j++)
            {
                row[j] = Double.Parse(dtMatrix.Rows[i][j + 1].ToString());
            }

            return row;
        }

        private double[] ColumnToArray(int i)
        {
            double[] column = new double[this.row];
            for (int j = 0; j < this.row; j++)
            {
                column[j] = Double.Parse(dtMatrix.Rows[j][i + 1].ToString());
            }

            return column;
        }
    }
}
